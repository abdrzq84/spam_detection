from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import nltk
from nltk.corpus import stopwords 
import string,re 
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from joblib import dump, load
import sys
from flask import Flask, redirect, url_for, render_template, request

#download stopword package
nltk.download('stopwords')
# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()
#read slangword data
slang_word_upper = pd.read_csv('slang_word_upper.csv', sep=';')
slang_word_lower = pd.read_csv('slang_word_lower.csv', sep=';')
slang_word_upper_key = slang_word_upper['0'].values.tolist()
slang_word_lower_key = slang_word_lower['0'].values.tolist()

#text cleaning(remove @tag,#,URL)
def strip_links(text):
    link_regex    = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
    links         = re.findall(link_regex, text)
    for link in links:
        text = text.replace(link[0], ', ')    
    return text

def strip_all_entities(text):
    text = re.sub(r'@(?<=@)\w+?\s', '', text) #remove mention
    text = re.sub(r'#(?<=#)\w+?\s','',text) #remove #
    text = re.sub(r'RT[\s]','',text) #remove RT

    words = []
    for word in text.split():
        word = word.strip()
        if word:
          # remove slangword
          if word in slang_word_upper_key:
            ##
            index = slang_word_upper_key.index(word)
            word = slang_word_upper['1'][index]
              #remove stopword
            if word.lower() not in stopwords.words('indonesian'):
              words.append(word)

          elif word in slang_word_lower_key:
            ##
            index = slang_word_lower_key.index(word)
            word = slang_word_lower['1'][index]
              #remove stopword
            if word.lower() not in stopwords.words('indonesian'):
                words.append(word)

          else:
              #remove stopword
              if word.lower() not in stopwords.words('indonesian'):
                words.append(word)
    return ' '.join(words)
    
#Preprocessing
def textclean(text):
  data = []
  for t in text:
    #steaming
    data.append(stemmer.stem(strip_all_entities(strip_links(t))))
  #remove duplicate
  data = list(dict.fromkeys(data))
  data = list(map(str.lower,data))
  return data

token = []
vectorizer = CountVectorizer()
clf = load('modelku.pkl')
data = pd.read_csv('dataset.csv', sep=';') 
matrix = vectorizer.fit_transform(data['Text'].values.astype('U'))

xtext = ["saya sedih sekali kuy"]
xtext = textclean(xtext)
vect = vectorizer.transform(xtext).toarray()
print(clf.predict(vect)==1)
hasil = clf.predict(vect)
print (hasil)

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        text_content = request.form['content']
        # list_text = []
        # list_text.append(text_content)
        xtext = textclean([text_content])
        vect = vectorizer.transform(xtext).toarray()
        hasil = clf.predict(vect)
        if hasil == 1:
            return render_template('index.html',hasil="Spam")
        elif hasil == 0:
            return render_template('index.html',hasil="Legit")
        else:
            return render_template('index.html',hasil=hasil)
    else: 
        return render_template('index.html')

if __name__ == '__main__':
    app.run()


